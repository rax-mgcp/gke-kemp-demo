# Purpose
A wiring test deployment to sanity check the infrastructure in lieu of actual customer code.

# Prerequisites
* A Kubernetes cluster in GKE (tested on v1.11.8-gke.6)
* ... with `nginx-ingress-controller` deployed
* A CloudSQL MySQL 5.7 instance
* The GKE node pool service account with the `Cloud SQL Client` role granted

# Usage
## Create an SQL user with minimal permissions on the database:
`grant show databases on *.* to gkekempdemo@'%' identified by 'YOUR_PASSWORD_HERE'`

If you'd like a user different than `gkekempdemo` you'll need to edit the `mysql_username` key in the config map defined in `kemp-demo.yaml`

## Update the `instance_connection_name` key in the config map defined in `kemp-demo.yaml` to match your CloudSQL instance
## Create the namespace for the deployment. 
The manifest has the appropriate config that does that, but we'll need the namespace to create a secret that contains the MySQL password. Alternatively uncomment the secret resource in `kemp-demo.yaml`.
`kubectl create namespace gke-kemp-demo`
## Create the MySQL password secret
`kubectl create secret generic gke-kemp-demo -n gke-kemp-demo --from-literal=mysql_password=YOUR_PASSWORD_HERE`
## Apply the deployment manifest
`kubectl apply -f k8s-manifests/`