<?php
header('Content-Type: text/plain');

$sql = new mysqli('127.0.0.1', $_ENV['MYSQL_USERNAME'], $_ENV['MYSQL_PASSWORD'], NULL);
if ($sql->connect_error) {
    echo('Connect Error (' . $sql->connect_errno . ') ' . $sql->connect_error);
} else {
  echo("Connected: OK\n");
}

echo("Databases:\n");
$result = $sql->query("show databases;");
print_r($result->fetch_assoc());

$sql->close();
?>
